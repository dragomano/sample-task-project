<?php

namespace core;

use core\classes\View;
use core\classes\Upload;

class BaseController extends Controller
{
    protected $title;

    public function __construct()
    {
        parent::__construct(new View);
    }

    // Логика главной страницы
    public function actionIndex()
    {
        global $pdo;

        $this->title = 'Задачник';

        // Текущая страница (по умолчанию - 1)
        $current_page = 1;

        // Столбец для сортировки по умолчанию
        $sorting = 'id';

        // Тип сортировки по умолчанию
        $sorting_type = $_SESSION['sorting_type'] ?: 'DESC';

        // Узнаем текущий вид сортировки и пагинации
        if (!empty($_REQUEST['action'])) {
            $actions = explode('/', $_REQUEST['action']);

            if (in_array('sort', $actions)) {
                $sorting = array_pop($actions);
                $sorting_type = $sorting_type == 'DESC' ? 'ASC' : 'DESC';
            }

            // Сохраняем вид сортировки
            $_SESSION['sorting'] = $sorting;

            // Сохраняем тип сортировки
            $_SESSION['sorting_type'] = $sorting_type;

            // Пагинация
            if (in_array('page', $actions)) {
                // Текущая страница
                $current_page = array_pop($actions);

                // Настраиваем переход на следующую страницу в пагинации
                if ($current_page == 'next') {
                    if (isset($_SESSION['pagination']) && ($_SESSION['pagination']['total_pages'] >= $_SESSION['pagination']['current_page'] + 1)) {
                        $current_page = $_SESSION['pagination']['current_page'] + 1;
                    } else {
                        header('Location:/');
                    }
                }

                // Настраиваем переход на предыдущую страницу в пагинации
                if ($current_page == 'prev') {
                    if (isset($_SESSION['pagination']) && ($_SESSION['pagination']['current_page'] - 1 > 0)) {
                        $current_page = $_SESSION['pagination']['current_page'] - 1;
                    } else {
                        header('Location:/');
                    }
                }
            }
        }

        // Узнаем количество записей в базе
        if (isset($_SESSION['count'])) {
            $count = $_SESSION['count'];
        } else {
            $count = $pdo->query("SELECT COUNT(*) as count FROM `tasks` AS t
            LEFT JOIN `users` AS u
            ON (u.id = t.user_id)")->fetchColumn();

            $_SESSION['count'] = $count;
        }

        // Отображаемое количество задач на одной странице
        $limit = 3;

        // Всего страниц в пагинации
        $total_pages = ceil($count / $limit);

        $current_page = (int) $current_page;

        // Если указана несуществующая страница, переходим на главную
        if (($current_page > $total_pages || $current_page < 1) && $count > 0)
            header('Location:/');

        $pagination = [
            'prev_page'    => $current_page - 1 > 0 ? $current_page - 1 : null,
            'current_page' => $current_page,
            'next_page'    => $current_page + 1 <= $total_pages ? $current_page + 1 : null,
            'total_tasks'  => $count,
            'total_pages'  => $total_pages
        ];

        $_SESSION['pagination'] = $pagination;

        // Определяем, с какой записи выводить
        $note = ($current_page * $limit) - $limit;

        // Получаем список всех задач, с пагинацией и сортировкой
        $sql = 'SELECT t.id, t.description, t.image, t.status, u.name, u.email
            FROM `tasks` AS t
            LEFT JOIN `users` AS u
            ON (u.id = t.user_id)
            ORDER BY ' . $sorting . ' ' . $sorting_type . ' LIMIT ?, ?';
        $query = $pdo->prepare($sql);
        $query->bindValue(1, $note, \PDO::PARAM_INT);
        $query->bindValue(2, $limit, \PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        // Получаем список пользователей для формы добавления новой задачи
        if (empty($_SESSION['users'])) {
            $sql = 'SELECT `id`, `name` FROM `users`';
            $query = $pdo->query($sql);
            $_SESSION['users'] = $query->fetchAll(\PDO::FETCH_ASSOC);
        }

        $content = $this->view->render('index', ['data' => $data, 'pagination' => $pagination], true);
        $this->render($content);
    }

    // Добавление задачи
    public function actionAddTask()
    {
        global $pdo;

        // Обрабатываем пользовательский ввод
        $desc  = htmlspecialchars($_POST['taskDesc']);
        $image = '';
        $user  = (int) $_POST['taskAssign'];

        // Загружаем картинку, если указана
        if (!empty($_FILES['image'])) {
            $img   = new Upload($_FILES['image']);
            $image = $img->upload();
        }

        try {
            $query = $pdo->prepare("INSERT INTO `tasks`
                SET `description` = :description, `image` = :image, `user_id` = :user_id, `status` = :status");
            $query->execute(array(
                'description' => $desc,
                'image'       => $image,
                'user_id'     => $user,
                'status'      => 0
            ));
        } catch (\PDOException $e) {
            echo 'Ошибка в запросе: ' . $e->getMessage() . '<br /><a href="/">Вернуться на главную страницу</a>';
            exit;
        }

        unset($_SESSION['count']);

        header('Location:' . $_SERVER['HTTP_REFERER']);
    }

    // Редактирование задачи
    public function actionEditTask()
    {
        global $pdo;

        if (empty($_POST['taskId']))
            return;

        // Обрабатываем пользовательский ввод
        $desc   = htmlspecialchars($_POST['taskDesc']);
        $status = (bool) $_POST['taskDone'];
        $id     = (int) $_POST['taskId'];

        $query = $pdo->prepare("UPDATE `tasks` SET `description` = :description, `status` = :status WHERE `id` = :id");
        $query->execute(array('description' => $desc, 'status' => (int) $status, 'id' => $id));

        header('Location:' . $_SERVER['HTTP_REFERER']);
    }

    // Удаление задачи
    public function actionRemoveTask()
    {
        global $pdo;

        $id = (int) $_POST['taskId'];

        $sql = "SELECT image FROM `tasks` WHERE `id` = ?";
        $query = $pdo->prepare($sql);
        $query->bindValue(1, $id, \PDO::PARAM_INT);
        $query->execute();
        $image = $query->fetchColumn();

        clearstatcache();

        // Удаляем картинку, связанную с задачей (если есть)
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $image;
        if (!empty($image) && file_exists($path))
            unlink($path);

        // Удаляем саму задачу
        $query = $pdo->prepare("DELETE FROM `tasks` WHERE `id` = :id");
        $query->execute(array('id' => $id));

        unset($_SESSION['count']);
    }

    // Авторизация
    public function actionLogin()
    {
        global $pdo;

        if (isset($_POST['auth'])) {
            $_SESSION['login'] = htmlspecialchars($_POST['login']);
            $_SESSION['password'] = md5(htmlspecialchars($_POST['password']));

            $sql = 'SELECT u.password, r.name
                FROM `users` AS u
                LEFT JOIN `roles` AS r
                ON (r.id = u.role_id)
                WHERE `login` = :login';
            $query = $pdo->prepare($sql);
            $query->execute(['login' => $_SESSION['login']]);
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);

            $login = $_SESSION['login'];
            $password = $result[0]['password'];
            $role = $result[0]['name'];

            if ($role == 'admin')
                $_SESSION['role'] = 'admin';
        }

        $iss = isset($_SESSION['login']) && isset($_SESSION['password']);
        if ($iss && $_SESSION['password'] === $password)
            $_SESSION['auth'] = true;

        header('Location:' . $_SERVER['HTTP_REFERER']);
    }

    // Деавторизация
    public function actionLogout()
    {
        unset($_SESSION['login']);
        unset($_SESSION['password']);
        unset($_SESSION['auth']);
        unset($_SESSION['role']);

        header('Location:/');
    }

    // Неизвестная страница
    public function action404()
    {
        parent::action404();
        $this->title = 'Страница не найдена - 404';

        $content = $this->view->render('404', [], true);
        $this->render($content);
    }

    protected function render($content)
    {
        $params = [];
        $params['title'] = $this->title;
        $params['content'] = $content;
        $this->view->render(MAIN_LAYOUT, $params);
    }
}
