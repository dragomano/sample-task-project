<?php

namespace core\classes;

class Db
{
    public static function connect()
    {
        global $pdo;

        require_once('config/config.php');

        try {
            $pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD, [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]);
        } catch (\PDOException $e) {
            echo 'Ошибка при подключении к базе данных!' . $e->getMessage() . '<br />';
        }
    }
}
