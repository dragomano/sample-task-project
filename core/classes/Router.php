<?php

namespace core\classes;

class Router
{
    public static function start()
    {
        $controller_name = 'core\Base';
        $action_name = 'index';

        $uri = explode('/', $_SERVER['REQUEST_URI']);

        if ($uri[1])
            $action_name = $uri[1];

        $controller_name = $controller_name . 'Controller';
        $action_name = 'action' . $action_name;
        $controller = new $controller_name();

        if (method_exists($controller, $action_name))
            $controller->$action_name();
        else
            $controller->action404();
    }
}
