<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title><?=$title?></title>
</head>
<body>
    <!-- Навигационная панель вверху -->
    <nav class="navbar navbar-dark bg-dark flex-md-nowrap">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">Задачник</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
<?php if ($_SESSION['auth']) { ?>
                <a class="nav-link" href='/logout'>Выйти</a>
<?php } else { ?>
                <form class="form-inline" action="/login" method="post">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputLogin">i</span>
                        </div>
                        <input type="text" name="login" class="form-control mr-sm-1" placeholder="Логин" aria-label="Логин" aria-describedby="inputLogin" required>
                    </div>
                    <input type="password" name="password" id="inputPassword" class="form-control mr-sm-1" placeholder="Пароль" required>
                    <button type="submit" name="auth" class="btn btn-primary">Войти</button>
                </form>
<?php } ?>
            </li>
        </ul>
    </nav>

    <!-- Модальная форма для добавления новой задачи -->
    <div class="modal" id="addTask" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Новая задача</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-signin" name="newTask" id="newTask" action="/addtask" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div id="previewArea"></div>
                        </div>
                        <div class="form-group">
                            <label for="inputTaskDesc">Описание</label>
                            <textarea name="taskDesc" id="inputTaskDesc" class="form-control" placeholder="Опишите задачу" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputTaskAssigned">Кому поручить</label>
                            <select name="taskAssign" id="inputTaskAssigned" class="form-control" required>
<?php if ($_SESSION['users']): ?>
<?php foreach($_SESSION['users'] as $row): ?>
                                <option value="<?=$row['id']?>"><?=$row['name']?></option>
<?php endforeach; ?>
<?php endif; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputImage">Картинка</label>
                            <input type="file" name="image" id="inputImage" accept="image/jpeg,image/png,image/gif">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-info" id="preview">Предварительный просмотр</button>
                    <button type="submit" form="newTask" class="btn btn-primary">Отправить</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Основная часть страницы -->
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-12 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Список задач</h1>
                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group mr-2">
                            <button data-toggle="modal" data-target="#addTask" class="btn btn-sm btn-outline-secondary">Добавить задачу</button>
                        </div>
                    </div>
                </div>

                <?=$content?>

            </main>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script>
         jQuery(document).ready(function($){
            $("#preview").on("click", function(){
                $("#previewArea").html("<label>Предварительный просмотр</label><div>" + $("#inputTaskDesc").val() + "</div>");
            });
            $(".del").on("click", function(){
                var id = $(this).attr("data-id");
                if (id) {
                    $.ajax({
                        type: "POST",
                        url: "/removetask",
                        data: {taskId: id}
                    }).done(function() {
                        window.location.href = "/";
                    });
                }
            });
        });
    </script>
</body>
</html>