<div class="table-responsive">
<?php
    $sorting = true;
    if (count($data) == 1 && $pagination['total_pages'] == 1)
        $sorting = false;
?>
<?php if ($data): ?>
    <table class="table table-striped table-bordered table-sm">
        <thead>
            <tr class="text-center">
                <th><?php if ($sorting) echo '<a href="/index/sort/id">';?>#<?php if ($sorting) echo '</a>';?></th>
                <th>Описание задачи</th>
                <th>Картинка</th>
                <th><?php if ($sorting) echo '<a href="/index/sort/name">';?>Имя пользователя<?php if ($sorting) echo '</a>';?></th>
                <th><?php if ($sorting) echo '<a href="/index/sort/email">';?>E-mail<?php if ($sorting) echo '</a>';?></th>
                <th><?php if ($sorting) echo '<a href="/index/sort/status">';?>Статус<?php if ($sorting) echo '</a>';?></th>
                <th>x</th>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($data as $row): ?>
            <tr>
                <td class="text-center"><?=$row['id']?></td>
                <td><?=$row['description']?><? if ($_SESSION['role'] == 'admin') { ?> <span class="badge badge-info float-right" data-toggle="modal" data-target="#editTask<?=$row['id']?>" style="cursor:pointer">Правка</span><? } ?>

                    <!-- Модальная форма для редактирования выбранной задачи -->
                    <div class="modal" id="editTask<?=$row['id']?>" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Редактирование задачи</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-signin" name="newTask" id="newTask<?=$row['id']?>" action="/edittask" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="taskId" value="<?=$row['id']?>">
                                        <div class="form-group">
                                            <label for="inputTaskDesc<?=$row['id']?>">Описание</label>
                                            <textarea name="taskDesc" id="inputTaskDesc<?=$row['id']?>" class="form-control" placeholder="Опишите задачу" required><?=$row['description']?></textarea>
                                        </div>
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" name="taskDone" id="taskDone<?=$row['id']?>"<?php if ($row['status']) echo ' checked'; ?>>
                                            <label class="form-check-label" for="taskDone<?=$row['id']?>">Задача выполнена</label>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                    <button type="submit" form="newTask<?=$row['id']?>" class="btn btn-primary">Отправить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="text-center"><?php if ($row['image']) echo '<img alt="" src="/uploads/' . $row['image'] . '" width="60">'; else echo 'Нет';?></td>
                <td><?=$row['name']?></td>
                <td><?=$row['email']?></td>
                <td class="text-center"><?php if ($row['status']) echo 'Готово'; else echo 'Не выполнено';?></td>
                <td class="text-center"><span class="del badge badge-danger" style="cursor:pointer" data-id="<?=$row['id']?>">Удалить</span></td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?php if (count($data) > 0): ?>
    <nav>
        <ul class="pagination pagination-sm justify-content-center">
        <?php if ($pagination['prev_page']): ?>
            <li class="page-item"><a class="page-link" href="/index/page/prev">Назад</a></li>
        <?php endif; ?>
        <?php for ($i = 1; $i <= $pagination['total_pages']; $i++): ?>
            <li class="page-item"><? if ($pagination['current_page'] <> $i): ?><a class="page-link" href="/index/page/<?=$i?>"><?= $i ?></a><? else: ?><span class="page-link"><?= $i ?></span><? endif; ?></li>
        <?php endfor; ?>
        <?php if ($pagination['next_page']): ?>
            <li class="page-item"><a class="page-link" href="/index/page/next">Далее</a></li>
        <?php endif; ?>
        </ul>
    </nav>
    <?php endif; ?>
<?php endif; ?>
</div>